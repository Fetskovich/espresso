package com.example.efetskovich.espressoexam.activities;

import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.efetskovich.espressoexam.constants.Constants;
import com.example.efetskovich.espressoexam.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * @author e.fetskovich on 8/3/17.
 */
@RunWith(AndroidJUnit4.class)
public class AsyncActivityTest {

    @Rule
    public ActivityTestRule<AsyncActivity> asyncTestRule = new ActivityTestRule<AsyncActivity>(AsyncActivity.class);

    @Test
    public void asyncTest() {
        Espresso.onView(withId(R.id.asyncText)).check(matches(withText(Constants.DATA_LOADER)));
    }

}