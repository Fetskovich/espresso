package com.example.efetskovich.espressoexam.activities;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.efetskovich.espressoexam.constants.Constants;
import com.example.efetskovich.espressoexam.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * @author e.fetskovich on 8/4/17.
 */
@RunWith(AndroidJUnit4.class)
public class DialogTestActivityTest {

    @Rule
    public ActivityTestRule<DialogTestActivity> rule = new ActivityTestRule<DialogTestActivity>(DialogTestActivity.class);

    @Test
    public void checkDialogDisplaying(){
        onView(withId(R.id.confirm_dialog_button)).perform(click());
        onView(withText(Constants.TITLE)).check(matches(isDisplayed()));
    }

    @Test
    public void checkDialogOkButton(){
        onView(withId(R.id.confirm_dialog_button)).perform(click());

        onView(withId(android.R.id.button1)).perform(click());

        onView(withId(R.id.status_text)).check(matches(withText(Constants.OK)));
    }

    @Test
    public void checkDialogNoButton(){
        onView(withId(R.id.confirm_dialog_button)).perform(click());

        onView(withId(android.R.id.button2)).perform(click());

        onView(withId(R.id.status_text)).check(matches(withText(Constants.CANCEL)));
    }

}