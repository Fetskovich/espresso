package com.example.efetskovich.espressoexam.activities;


import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.efetskovich.espressoexam.R;
import com.example.efetskovich.espressoexam.constants.Constants;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@RunWith(AndroidJUnit4.class)
public class ViewPagerTest {

    @Rule
    public ActivityTestRule<ViewPagerTestActivity> mActivityTestRule = new ActivityTestRule<>(ViewPagerTestActivity.class);

    @Test
    public void viewPagerTest() {
        onView(allOf(ViewMatchers.withId(R.id.viewPager), isDisplayed())).perform(swipeLeft());

        onView(allOf(withId(R.id.viewPager), isDisplayed())).perform(swipeRight()).perform(swipeRight());

        onView(allOf(withId(R.id.viewPager), isDisplayed())).perform(swipeLeft());

        onView(allOf(withId(R.id.viewPager), isDisplayed())).perform(swipeLeft());
        onView(withText(Constants.PAGE + " 3")).check(matches(isDisplayed()));
    }

}
