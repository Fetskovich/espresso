package com.example.efetskovich.espressoexam.activities;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.efetskovich.espressoexam.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.example.efetskovich.espressoexam.matchers.EspressoTestMatcher.withDrawable;

/**
 * @author e.fetskovich on 8/8/17.
 */
@RunWith(AndroidJUnit4.class)
public class BitmapCompareActivityTest {

    @Rule
    public ActivityTestRule<BitmapCompareActivity> rule = new ActivityTestRule<BitmapCompareActivity>(BitmapCompareActivity.class);

    @Test
    public void testOnBitmapCompare() {
        onView(withId(R.id.bitmapTest)).check(matches(isDisplayed()));
        onView(withId(R.id.bitmapTest)).check(matches(withDrawable(R.drawable.ic_launcher)));
    }

}