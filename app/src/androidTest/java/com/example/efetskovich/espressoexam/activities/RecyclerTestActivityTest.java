package com.example.efetskovich.espressoexam.activities;

import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import com.example.efetskovich.espressoexam.constants.Constants;
import com.example.efetskovich.espressoexam.R;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

/**
 * @author e.fetskovich on 8/3/17.
 */
@RunWith(AndroidJUnit4.class)
public class RecyclerTestActivityTest {

    private final int ITEM = 18;

    @Rule
    public ActivityTestRule<RecyclerTestActivity> activityRule = new ActivityTestRule<>(
            RecyclerTestActivity.class);

    @Test
    public void scrollToItemBelowFold_checkItsText() {
        onView(ViewMatchers.withId(R.id.recyclerView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(ITEM, clickChildViewWithId(R.id.textView)));

        String itemElementText =
                Constants.ITEM + "" + String.valueOf(ITEM);

        onView(withText(itemElementText)).check(matches(isDisplayed()));
    }

    @Test
    public void checkShowingToastText() {
        onView(ViewMatchers.withId(R.id.recyclerView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(ITEM, clickChildViewWithId(R.id.textView)));

        String itemElementText =
                Constants.ITEM + "" + String.valueOf(ITEM);

        View decorView = activityRule.getActivity().getWindow().getDecorView();

        onView(withText(itemElementText)).inRoot(withDecorView(not(is(decorView)))).check(matches(isDisplayed()));
    }

    private static ViewAction clickChildViewWithId(final int id) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return "Click on a child view with specified id.";
            }

            @Override
            public void perform(UiController uiController, View view) {
                View v = view.findViewById(id);
                v.performClick();
            }
        };
    }

}