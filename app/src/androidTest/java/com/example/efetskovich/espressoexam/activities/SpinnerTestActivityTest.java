package com.example.efetskovich.espressoexam.activities;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import com.example.efetskovich.espressoexam.constants.Constants;

import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.not;

/**
 * @author e.fetskovich on 8/3/17.
 */
@RunWith(AndroidJUnit4.class)
public class SpinnerTestActivityTest {

    private final int CURRENT_ITEM = 3;

    @Rule
    public ActivityTestRule<SpinnerTestActivity> activityTestRule = new ActivityTestRule<SpinnerTestActivity>(SpinnerTestActivity.class);

    @Test
    public void chooseElementTest() {
        String itemElement = Constants.ITEM + "" + CURRENT_ITEM;

        onData(
                allOf(
                        is(instanceOf(String.class)),
                        is(itemElement))
        ).perform(click());

        View decorView = activityTestRule.getActivity().getWindow().getDecorView();

        onView(withText(itemElement)).inRoot(withDecorView(not(Matchers.is(decorView)))).check(matches(isDisplayed()));
    }

}