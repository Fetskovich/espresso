package com.example.efetskovich.espressoexam.matchers;

import android.view.View;

import org.hamcrest.Matcher;

/**
 * @author e.fetskovich on 8/8/17.
 */

public class EspressoTestMatcher {
    public static Matcher<View> withDrawable(final int resourceId) {
        return new DrawableMatcher(resourceId);
    }

    public static Matcher<View> noDrawable() {
        return new DrawableMatcher(-1);
    }
}
