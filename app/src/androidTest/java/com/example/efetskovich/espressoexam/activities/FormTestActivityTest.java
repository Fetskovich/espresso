package com.example.efetskovich.espressoexam.activities;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.efetskovich.espressoexam.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * @author e.fetskovich on 8/3/17.
 */
@RunWith(AndroidJUnit4.class)
public class FormTestActivityTest {

    @Rule
    public ActivityTestRule<FormTestActivity> formTest = new ActivityTestRule<FormTestActivity>(FormTestActivity.class);

    @Test
    public void fillFormNameEditText(){
        String name = "Form Nam";
        onView(withId(R.id.formName)).perform(typeText(name), closeSoftKeyboard());
        onView(withId(R.id.formName)).check(matches(withText(name)));
        onView(withId(R.id.formName)).perform(clearText());
    }

    @Test
    public void fillFormAgeEditText(){
        String age = String.valueOf(24);
        onView(withId(R.id.formAge)).perform(typeText(age), closeSoftKeyboard());
        onView(withId(R.id.formAge)).check(matches(withText(age)));
        onView(withId(R.id.formAge)).perform(clearText());
    }

    @Test
    public void fillFormTotalTextEditText(){
        String someText = "A lot of some text etc.";
        onView(withId(R.id.formSomeText)).perform(typeText(someText), closeSoftKeyboard());
        onView(withId(R.id.formSomeText)).check(matches(withText(someText)));
        onView(withId(R.id.formSomeText)).perform(clearText());
    }


    @Test
    public void fillEditText_CheckTotalText() {
        String name = "Form Nam";
        String age = String.valueOf(24);
        String someText = "A lot of some text etc.";

        onView(withId(R.id.formName))
                .perform(typeText(name), closeSoftKeyboard());

        onView(withId(R.id.formAge))
                .perform(typeText(age), closeSoftKeyboard());

        onView(withId(R.id.formSomeText))
                .perform(typeText(someText), closeSoftKeyboard());

        onView(withId(R.id.formSaveData)).perform(click());

        onView(withId(R.id.formTotalText)).check(matches(withText(name + " " + age + " " + someText)));
    }


}