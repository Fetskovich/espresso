package com.example.efetskovich.espressoexam;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.efetskovich.espressoexam.activities.AsyncActivity;
import com.example.efetskovich.espressoexam.activities.BitmapCompareActivity;
import com.example.efetskovich.espressoexam.activities.DialogTestActivity;
import com.example.efetskovich.espressoexam.activities.FormTestActivity;
import com.example.efetskovich.espressoexam.activities.IntentTestActivity;
import com.example.efetskovich.espressoexam.activities.RecyclerTestActivity;
import com.example.efetskovich.espressoexam.activities.SpinnerTestActivity;
import com.example.efetskovich.espressoexam.activities.TakeCameraPictureActivity;
import com.example.efetskovich.espressoexam.activities.ViewPagerTestActivity;

public class MainActivity extends AppCompatActivity {

    private Button spinnerButton;
    private Button recyclerButton;
    private Button formButton;
    private Button intentButton;
    private Button asyncButton;
    private Button cameraButton;
    private Button dialogButton;
    private Button viewPagerButton;
    private Button compareBitmapButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initButtons();
        initButtonsListeners();
    }

    private void initButtons() {
        spinnerButton = (Button) findViewById(R.id.spinnerTest);
        recyclerButton = (Button) findViewById(R.id.recyclerTest);
        formButton = (Button) findViewById(R.id.formTest);
        intentButton = (Button) findViewById(R.id.intentTest);
        asyncButton = (Button) findViewById(R.id.asyncTest);
        cameraButton = (Button) findViewById(R.id.cameraTest);
        dialogButton = (Button) findViewById(R.id.dialogTest);
        viewPagerButton = (Button) findViewById(R.id.viewPagerTest);
        compareBitmapButton = (Button) findViewById(R.id.compareBitmapTest);
    }

    private void initButtonsListeners() {
        spinnerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(SpinnerTestActivity.class);
            }
        });

        recyclerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(RecyclerTestActivity.class);
            }
        });

        formButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(FormTestActivity.class);
            }
        });

        intentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(IntentTestActivity.class);
            }
        });

        asyncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(AsyncActivity.class);
            }
        });
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(TakeCameraPictureActivity.class);
            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(DialogTestActivity.class);
            }
        });

        viewPagerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(ViewPagerTestActivity.class);
            }
        });

        compareBitmapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(BitmapCompareActivity.class);
            }
        });

    }


    private void startActivity(Class clazz) {
        Intent intent = new Intent(this, clazz);
        startActivity(intent);
    }

}
