package com.example.efetskovich.espressoexam.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.efetskovich.espressoexam.constants.Constants;
import com.example.efetskovich.espressoexam.R;
import com.example.efetskovich.espressoexam.adapters.RecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

public class RecyclerTestActivity extends AppCompatActivity {

    private final int ITEMS_COUNT = 50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_test);

        initRecyclerView();

    }

    private void initRecyclerView() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        List<String> dataSet = new ArrayList<>(ITEMS_COUNT);
        for (int i = 0; i < ITEMS_COUNT; i++) {
            dataSet.add(Constants.ITEM + "" + i);
        }
        RecyclerAdapter adapter = new RecyclerAdapter(dataSet, getApplicationContext());
        recyclerView.setAdapter(adapter);
    }

}
