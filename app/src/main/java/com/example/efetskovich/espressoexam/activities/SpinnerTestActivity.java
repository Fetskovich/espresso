package com.example.efetskovich.espressoexam.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.efetskovich.espressoexam.constants.Constants;
import com.example.efetskovich.espressoexam.R;

import java.util.ArrayList;
import java.util.List;

public class SpinnerTestActivity extends AppCompatActivity {

    private Spinner spinner;
    private final int ITEMS_COUNT = 20;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner_test);

        initSpinner();
    }

    private void initSpinner() {
        spinner = (Spinner) findViewById(R.id.spinner);

        final List<String> dataSet = new ArrayList<>(ITEMS_COUNT);
        for (int i = 0; i < ITEMS_COUNT; i++) {
            dataSet.add(Constants.ITEM + "" + i);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dataSet);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Toast.makeText(getBaseContext(), dataSet.get(position), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // do nothing
            }
        });
    }

}
