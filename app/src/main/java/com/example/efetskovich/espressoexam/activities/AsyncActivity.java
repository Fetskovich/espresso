package com.example.efetskovich.espressoexam.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.efetskovich.espressoexam.constants.Constants;
import com.example.efetskovich.espressoexam.R;

public class AsyncActivity extends AppCompatActivity {

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async);
        initComponents();
    }

    @Override
    protected void onResume() {
        super.onResume();
        createAsyncTask().execute();
    }

    private AsyncTask<Void, Void, Void> createAsyncTask() {
        return new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPostExecute(Void aVoid) {
                textView.setText(Constants.DATA_LOADER);
            }

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }
        };
    }

    private void initComponents() {
        textView = (TextView) findViewById(R.id.asyncText);
    }

}
