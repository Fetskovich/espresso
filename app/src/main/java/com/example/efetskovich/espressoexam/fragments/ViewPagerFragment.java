package com.example.efetskovich.espressoexam.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.efetskovich.espressoexam.R;

/**
 * @author e.fetskovich on 8/8/17.
 */

public class ViewPagerFragment extends Fragment {
    private String title;
    private int page;

    public static ViewPagerFragment newInstance(int page, String title) {
        ViewPagerFragment fragmentFirst = new ViewPagerFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("someInt", 0);
        title = getArguments().getString("someTitle");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_pager, container, false);
        TextView fragmentNumber = (TextView) view.findViewById(R.id.fragmentNumber);
        fragmentNumber.setText(title);
        return view;
    }


}
