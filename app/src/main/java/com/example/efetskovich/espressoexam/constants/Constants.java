package com.example.efetskovich.espressoexam.constants;

/**
 * @author e.fetskovich on 8/7/17.
 */

public class Constants {
    // AsyncActivity
    public static final String DATA_LOADER = "Data loader";

    // DialogTestActivity
    public static final String MESSAGE = "Message";
    public static final String TITLE = "Title";
    public static final String OK = "Ok";
    public static final String CANCEL = "Cancel";

    // RecyclerTestActivity
    public static final String ITEM = "Item: ";

    // ViewPagerActivity
    public static final String PAGE = "Page # ";


}
