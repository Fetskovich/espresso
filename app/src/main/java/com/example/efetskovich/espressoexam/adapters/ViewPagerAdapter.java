package com.example.efetskovich.espressoexam.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.efetskovich.espressoexam.constants.Constants;
import com.example.efetskovich.espressoexam.fragments.ViewPagerFragment;

/**
 * @author e.fetskovich on 8/8/17.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 3;


    public ViewPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public Fragment getItem(int position) {
        return ViewPagerFragment.newInstance(position, Constants.PAGE+" "+(position+1));
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }

}

