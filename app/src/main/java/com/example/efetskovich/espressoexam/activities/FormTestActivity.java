package com.example.efetskovich.espressoexam.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.efetskovich.espressoexam.R;

public class FormTestActivity extends AppCompatActivity {

    private TextView totalText;

    private EditText nameText;
    private EditText ageText;
    private EditText someText;

    private Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_test);

        initComponents();
        initButtonClickListener();
    }

    private void initComponents() {
        totalText = (TextView) findViewById(R.id.formTotalText);
        nameText = (EditText) findViewById(R.id.formName);
        ageText = (EditText) findViewById(R.id.formAge);
        someText = (EditText) findViewById(R.id.formSomeText);
        saveButton = (Button) findViewById(R.id.formSaveData);
    }

    private void initButtonClickListener() {
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                totalText.setText(getTotalTextFromEdits());
            }
        });
    }

    private String getTotalTextFromEdits() {
        return nameText.getText() + " " + ageText.getText()  + " " + someText.getText();
    }

}
