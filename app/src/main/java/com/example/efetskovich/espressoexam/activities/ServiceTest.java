package com.example.efetskovich.espressoexam.activities;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import java.util.Random;

/**
 * @author e.fetskovich on 8/7/17.
 */

public class ServiceTest extends Service {

    public static final String SEED_KEY = "SEED_KEY";

    private final IBinder binder = new LocalBinder();

    private Random randomGenerator = new Random();

    private long seed;

    @Override
    public IBinder onBind(Intent intent) {
        if (intent.hasExtra(SEED_KEY)) {
            seed = intent.getLongExtra(SEED_KEY, 0);
            randomGenerator.setSeed(seed);
        }
        return binder;
    }

    public class LocalBinder extends Binder {

        public ServiceTest getService() {
            return ServiceTest.this;
        }
    }

    public int getRandomInt() {
        return randomGenerator.nextInt(100);
    }
}
